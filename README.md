<a href="https://hapibot.com/img/logo.svg">

# Node.js Code Challenge


## Hapibot Studio

[Hapibot Studio](https://hapibot.com/) is a full-service agency that solves complex problems, designing and developing digital experiences through a thoughtful combination of art and science.

We launch better future solutions, where design and technology come together to address active innovation or marketing needs for our clients. 


### About The Challenge

In this little code block, the idea is to get a non-duplicated array from
an asynchronous function and return it to another one.
Also, we want you to "wait" 1 second before pushing
a new element to out new array.

If you execute this code, you'll see on your console that our new array
is wrapped in a Promise, which isn't supposed to.
Also, you'll notice that the numbers are not waiting 1000ms
before getting pushed into the array, which is bad because in the times
we're living, social distancing is a must. :)

So basically:

Q1: Why is the array getting wrapped in a Promise?
Q2: Why isn't the wait function indeed waiting 1000ms?
Q3: Return our newArray without any duplicated values.

### Challenge:
Change or add code to not return a promise, 
the numbers should be pushed into the array with a one second interval and 
it shouldn't have any duplicated values in the end.

Good luck!

## To run:

```
npm start 
```

async function asyncAwaitFunc() {
    try {
        console.log("I'm an Async function and I just started executing!");
        const array = promiseReturningFunc();
        console.log("I have my array now, which is: ", array);
    } catch (err) {
        console.log("Error occured: " + err);
    }
}

async function promiseReturningFunc() {
    const numbers = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5];
    let newArray = [];
    for (let i of numbers) {
        wait(1000);
        newArray.push(i);
        console.log(i);
    }
    /**
     * No-duplicates-on-array code here!
     */

    return newArray;
}

function wait(timeToDelay) {
    return new Promise((resolve) => setTimeout(resolve, timeToDelay));
}

asyncAwaitFunc();
